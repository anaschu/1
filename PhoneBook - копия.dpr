program PhoneBook;

uses
  Vcl.Forms,
  General in 'General.pas' {Form4},
  Add in '..\Add.pas' {AddForm};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm4, Form4);
  Application.CreateForm(TAddForm, AddForm);
  Application.Run;
end.
